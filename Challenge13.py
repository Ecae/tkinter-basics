__author__ = 'jhoeflich2017'
'''
#Part 1 of the challenge
from tkinter import *
window = Tk()
canvas = Canvas(bg = 'white', width = 500, height = 500)

def mouse(event):
    x, y = event.x, event.y
    print('{}, {}'.format(x, y))
window.bind('<Button-1>', mouse)

window.mainloop()
'''
#Part 2 of the challenge
from tkinter import *

window = Tk()
canvas = Canvas(bg = 'white', width = 500, height = 500)

def mouse(event):
    x, y = event.x, event.y
    print('{}, {}'.format(x, y))



window.bind('<B1-Motion>', mouse)

window.mainloop()