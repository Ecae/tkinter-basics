__author__ = 'jhoeflich2017'

from tkinter import *

class Ball:
    def __init__(self, canvas, x1, y1, x2, y2):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.canvas = canvas
        self.ball = canvas.create_oval(self.x1, self.y1, self.x2, self.y2, fill="red")

        self.canvas.bind("<Up>", self.up)
        self.canvas.bind("<Down>", self.down)
        self.canvas.bind("<Left>", self.left)
        self.canvas.bind("<Right>", self.right)
        self.canvas.focus_set()

    def up(self, event):
        y = 0
        y -= 10
        self.canvas.move(self.ball, 0, y)

    def down(self, event):
        y = 0
        y += 10
        self.canvas.move(self.ball, 0, y)

    def left(self, event):
        x = 0
        x -= 10
        self.canvas.move(self.ball, x, 0)

    def right(self, event):
        x = 0
        x += 10
        self.canvas.move(self.ball, x, 0)




#Create Window
window = Tk()
window.title("Balls")
canvas = Canvas(window, width = 1000, height = 1000)
canvas.pack()

#Create ball
ball = Ball(canvas, 10, 10, 30, 30)


window.mainloop()
